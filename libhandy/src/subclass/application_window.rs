use glib::subclass::prelude::*;

use crate::ApplicationWindow;

pub trait ApplicationWindowImpl:
    gtk::subclass::application_window::ApplicationWindowImpl + 'static
{
}

unsafe impl<T: ApplicationWindowImpl> IsSubclassable<T> for ApplicationWindow {
    fn class_init(class: &mut glib::Class<Self>) {
        <gtk::ApplicationWindow as IsSubclassable<T>>::class_init(class);
    }

    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <gtk::ApplicationWindow as IsSubclassable<T>>::instance_init(instance);
    }
}
