// Copyright 2018–2021, The libhandy-rs Project Developers.
// See the COPYRIGHT file at the top-level directory of this distribution.
// Licensed under the LGPL-2.1-only, see the LICENSE file or <https://opensource.org/licenses/LGPL-2.1>

#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![allow(deprecated)]
#![allow(dead_code)]

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("Libhandy may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libhandy.");
            }
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
#[allow(clippy::wrong_self_convention)]
#[allow(unused_imports)]
mod auto;
mod value_object;

#[macro_use]
pub mod subclass;
pub mod prelude;

pub use auto::*;
pub use glib::Error;
pub use prelude::*;
pub use value_object::*;
