#!/bin/sh

for file in $(find libhandy libhandy-sys/ -name '*.rs'); do
    sed -i '/feature = "v2/d' "$file"
    sed -i '/feature = "v3/d' "$file"
done

if [ "$(tail -n1 libhandy/src/auto/deck.rs)" != "use gtk::Widget;" ]; then
    echo "use gtk::Widget;" >> libhandy/src/auto/deck.rs
fi

cargo fmt
